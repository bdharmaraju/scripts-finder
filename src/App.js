import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

import { Container, Row, Col } from 'react-bootstrap';
import Sidebar from './components/Sidebar';
import ScriptDetails from './components/ScriptDetails';
import ScriptActions from './components/ScriptActions';
import { Notifications } from 'react-push-notification';
function App(props) {
    const [script, setScript] = useState({});

    {/* to store the script coming from sidebar component */}
    function handleScript(script) {
        setScript({
            scriptInfo: script,
        });
    }
    
    {/* to update the changed script item */}
    function onScriptChange(script,isFav){
        script.isFav = isFav;
        setScript({
            scriptInfo: script,
        });
    }
    return (
        <div className="App">
            {/* Push Notification */}
            <div className="push-notification-card">
                <Notifications />
            </div>
            

            <Row className="mx-0">
                <Col xs="12" lg="2" className="px-0">
                    <Sidebar onSelectScript={handleScript} />
                </Col>
                <Col xs="12" lg="8" className="px-0">
                    <ScriptDetails selectedScript={script.scriptInfo} onScriptEdit= {onScriptChange} />
                </Col>
                <Col xs="12" lg="2" className="px-0">
                    <ScriptActions />
                </Col>
            </Row>
            <div className=""></div>
        </div>
    );
}

export default App;
