import React, { useState, useEffect } from 'react';

import FormControl from 'react-bootstrap/FormControl';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import Badge from 'react-bootstrap/Badge';
import Dropdown from 'react-bootstrap/Dropdown';
import ButtonGroup from 'react-bootstrap/ButtonGroup';


function Sidebar(props) {

    const [scriptsData, setScripts] = useState({
        scripts: [
            {
                id: '1',
                name: "List Item 1",
                labels: [
                    {
                        title: 'Label #1',
                        id: 2128781332,
                    },
                    {
                        title: 'Label #2',
                        id: 53546787856,
                    }
                ],
                owner: "Manher",
                lastUpdated: "21/05/2016",
                createdBy: "Dr",
                createdDate: "19/08/2019",
                source: "C:/Users/Deskptop/urna_leo_convallis.py",
                usage: "urna_leo_convallis.py [...args...]",
                sampleInvocation: [
                    "To make all pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna Urna_leo_convallis.py label1",
                    "To restore dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit Urna_leo_convallis.py label2 --urna"
                ],
                desc: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci eu lobortis elementum nibh tellus. Condimentum id venenatis a condimentum vitae. In dictum non consectetur a erat nam at lectus urna. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Parturient montes nascetur ridiculus mus mauris. Auctor augue mauris augue neque gravida in. Sed augue lacus viverra vitae congue eu consequat ac. Bibendum neque egestas congue quisque egestas diam in arcu. Sit amet porttitor eget dolor morbi non arcu risus quis. Leo vel orci porta non pulvinar neque laoreet suspendisse. Congue nisi vitae suscipit tellus.</p><p>Dignissim convallis aenean et tortor at. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Volutpat maecenas volutpat blandit aliquam etiam. Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Sem integer vitae justo eget magna fermentum. Sociis natoque penatibus et magnis dis. Sem integer vitae justo eget. Mi ipsum faucibus vitae aliquet. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Orci eu lobortis elementum nibh. At quis risus sed vulputate. Adipiscing elit ut aliquam purus sit amet luctus venenatis. Aliquam eleifend mi in nulla posuere sollicitudin. Odio eu feugiat pretium nibh ipsum consequat nisl. A lacus vestibulum sed arcu non odio euismod lacinia. Eget duis at tellus at urna condimentum mattis pellentesque id. Mollis nunc sed id semper risus in hendrerit gravida. Velit dignissim sodales ut eu sem integer vitae justo. Quis vel eros donec ac odio. Morbi leo urna molestie at elementum eu facilisis sed odio.</p><p>Aenean et tortor at risus viverra adipiscing at. Sagittis id consectetur purus ut. Ullamcorper sit amet risus nullam eget. Est sit amet facilisis magna etiam. Arcu dui vivamus arcu felis bibendum ut tristique. Sed odio morbi quis commodo odio aenean sed adipiscing.</p>",
                commands: [{
                    command: "--urna <urna >",
                    text: "urna feugiat",
                },
                {
                    command: "--morbi <morbi >",
                    text: "Ullamcorper sit amet risus nullam eget",
                },
                {
                    command: "--expl <expelleriamusr>",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                }, {
                    command: "--crucio <cruciatus>",
                    text: "Suspendisse sed nisi lacus sed viverra tellus",
                },
                {
                    command: "---expecto_patronum",
                    text: "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit.",
                },
                {
                    command: "--no_accio",
                    text: "Gravida in fermentum et sollicitudin ac orci phasellus egestas tellus",
                },
                {
                    command: "-n, --nisi",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },
                {
                    command: "--nisi_all",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                }
                ],
                isFav: true,
                isActive:true
            },
            {
                id: '3',
                name: "List Item 3",
                labels: [
                    {
                        title: 'Label #3',
                        id: 422432322,
                    },
                    {
                        title: 'Label #4',
                        id: 7875454878,
                    }
                ],
                owner: "Nebt",
                lastUpdated: "02/23/2017",
                createdBy: "Dr",
                createdDate: "11/07/2018",
                source: "C:/Users/Deskptop/morbi.py",
                usage: "morbi.py [...args...]",
                sampleInvocation: [
                    "To make all pellentesque habitant hjkfh ioueuieu nldhnksdj k morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna Urna_leo_convallis.py label1-ejkwjekjw",
                    "To restore kjdgj jdhjhuwj dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit Urna_leo_convallis.py label2 --urna jklrjekrjejjj kjk jk "
                ],
                desc: "<p>Lorem ipsum dolor  hkhf  jklj kfj uoiwueu woie89 08e0- 290 289 -09 e-0w 09 0wkel  sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci eu lobortis elementum nibh tellus. Condimentum id venenatis a condimentum vitae. In dictum non consectetur a erat nam at lectus urna. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Parturient montes nascetur ridiculus mus mauris. Auctor augue mauris augue neque gravida in. Sed augue lacus viverra vitae congue eu consequat ac. Bibendum neque egestas congue quisque egestas diam in arcu. Sit amet porttitor eget dolor morbi non arcu risus quis. Leo vel orci porta non pulvinar neque laoreet suspendisse. Congue nisi vitae suscipit tellus.</p><p>Dignissim convallis aenean et tortor at. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Volutpat maecenas volutpat blandit aliquam etiam. Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Sem integer vitae justo eget magna fermentum. Sociis natoque penatibus et magnis dis. Sem integer vitae justo eget. Mi ipsum faucibus vitae aliquet. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Orci eu lobortis elementum nibh. At quis risus sed vulputate. Adipiscing elit ut aliquam purus sit amet luctus venenatis. Aliquam eleifend mi in nulla posuere sollicitudin. Odio eu feugiat pretium nibh ipsum consequat nisl. A lacus vestibulum sed arcu non odio euismod lacinia. Eget duis at tellus at urna condimentum mattis pellentesque id. Mollis nunc sed id semper risus in hendrerit gravida. Velit dignissim sodales ut eu sem integer vitae justo. Quis vel eros donec ac odio. Morbi leo urna molestie at elementum eu facilisis sed odio.</p><p>Aenean et tortor at risus viverra adipiscing at. Sagittis id consectetur purus ut. Ullamcorper sit amet risus nullam eget. Est sit amet facilisis magna etiam. Arcu dui vivamus arcu felis bibendum ut tristique. Sed odio morbi quis commodo odio aenean sed adipiscing.</p>",
                commands: [{
                    command: "--expl <expelleriamusr>",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--morbi <morbi >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--urna <urna >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                }, {
                    command: "--crucio <cruciatus>",
                    text: "Ullamcorper sit amet risus nullam eget",
                },
                {
                    command: "---expecto_patronum",
                    text: "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit.",
                },
                {
                    command: "--no_accio",
                    text: "Lectus vestibulum mattis ullamcorper velit seds",
                },
                {
                    command: "-n, --nisi",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },
                {
                    command: "--nisi_all",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },

                ],
                isFav: false,
                isActive:false
            },
            {
                id: '4',
                name: "List Item 4",
                labels: [
                    {
                        title: 'Label #6',
                        id: 3535313233,
                    },
                    {
                        title: 'Label #7',
                        id: 653232886464,
                    }
                ],
                owner: "Reny",
                lastUpdated: "11/3/2013",
                createdBy: "Mark",
                createdDate: "10/09/2016",
                source: "C:/Users/Deskptop/morbi.py",
                usage: "morbi.py [...args...]",
                sampleInvocation: [
                    "To make all pellentesque habitant hjkfh ioueuieu nldhnksdj k morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna Urna_leo_convallis.py label1-ejkwjekjw",
                    "To restore kjdgj jdhjhuwj dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit Urna_leo_convallis.py label2 --urna jklrjekrjejjj kjk jk "
                ],
                desc: "<p>Lorem ipsum dolor  hkhf  jklj kfj uoiwueu woie89 08e0- 290 289 -09 e-0w 09 0wkel  sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci eu lobortis elementum nibh tellus. Condimentum id venenatis a condimentum vitae. In dictum non consectetur a erat nam at lectus urna. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Parturient montes nascetur ridiculus mus mauris. Auctor augue mauris augue neque gravida in. Sed augue lacus viverra vitae congue eu consequat ac. Bibendum neque egestas congue quisque egestas diam in arcu. Sit amet porttitor eget dolor morbi non arcu risus quis. Leo vel orci porta non pulvinar neque laoreet suspendisse. Congue nisi vitae suscipit tellus.</p><p>Dignissim convallis aenean et tortor at. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Volutpat maecenas volutpat blandit aliquam etiam. Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Sem integer vitae justo eget magna fermentum. Sociis natoque penatibus et magnis dis. Sem integer vitae justo eget. Mi ipsum faucibus vitae aliquet. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Orci eu lobortis elementum nibh. At quis risus sed vulputate. Adipiscing elit ut aliquam purus sit amet luctus venenatis. Aliquam eleifend mi in nulla posuere sollicitudin. Odio eu feugiat pretium nibh ipsum consequat nisl. A lacus vestibulum sed arcu non odio euismod lacinia. Eget duis at tellus at urna condimentum mattis pellentesque id. Mollis nunc sed id semper risus in hendrerit gravida. Velit dignissim sodales ut eu sem integer vitae justo. Quis vel eros donec ac odio. Morbi leo urna molestie at elementum eu facilisis sed odio.</p><p>Aenean et tortor at risus viverra adipiscing at. Sagittis id consectetur purus ut. Ullamcorper sit amet risus nullam eget. Est sit amet facilisis magna etiam. Arcu dui vivamus arcu felis bibendum ut tristique. Sed odio morbi quis commodo odio aenean sed adipiscing.</p>",
                commands: [{
                    command: "--expl <expelleriamusr>",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--morbi <morbi >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--urna <urna >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                }, {
                    command: "--crucio <cruciatus>",
                    text: "Ullamcorper sit amet risus nullam eget",
                },
                {
                    command: "---expecto_patronum",
                    text: "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit.",
                },
                {
                    command: "--no_accio",
                    text: "Lectus vestibulum mattis ullamcorper velit seds",
                },
                {
                    command: "-n, --nisi",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },
                {
                    command: "--nisi_all",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },

                ],
                isFav: false,
                isActive:false
            },
            {
                id: '12',
                name: "List Item 12",
                labels: [
                    {
                        title: 'Label #19',
                        id: 32346565665,
                    },
                    {
                        title: 'Label #21',
                        id: 57090434454676898,
                    }
                ],
                owner: "Manher",
                lastUpdated: "21/05/2016",
                createdBy: "Dr",
                createdDate: "19/08/2019",
                source: "C:/Users/Deskptop/urna_leo_convallis.py",
                usage: "urna_leo_convallis.py [...args...]",
                sampleInvocation: [
                    "To make all pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna Urna_leo_convallis.py label1",
                    "To restore dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit Urna_leo_convallis.py label2 --urna"
                ],
                desc: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci eu lobortis elementum nibh tellus. Condimentum id venenatis a condimentum vitae. In dictum non consectetur a erat nam at lectus urna. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Parturient montes nascetur ridiculus mus mauris. Auctor augue mauris augue neque gravida in. Sed augue lacus viverra vitae congue eu consequat ac. Bibendum neque egestas congue quisque egestas diam in arcu. Sit amet porttitor eget dolor morbi non arcu risus quis. Leo vel orci porta non pulvinar neque laoreet suspendisse. Congue nisi vitae suscipit tellus.</p><p>Dignissim convallis aenean et tortor at. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Volutpat maecenas volutpat blandit aliquam etiam. Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Sem integer vitae justo eget magna fermentum. Sociis natoque penatibus et magnis dis. Sem integer vitae justo eget. Mi ipsum faucibus vitae aliquet. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Orci eu lobortis elementum nibh. At quis risus sed vulputate. Adipiscing elit ut aliquam purus sit amet luctus venenatis. Aliquam eleifend mi in nulla posuere sollicitudin. Odio eu feugiat pretium nibh ipsum consequat nisl. A lacus vestibulum sed arcu non odio euismod lacinia. Eget duis at tellus at urna condimentum mattis pellentesque id. Mollis nunc sed id semper risus in hendrerit gravida. Velit dignissim sodales ut eu sem integer vitae justo. Quis vel eros donec ac odio. Morbi leo urna molestie at elementum eu facilisis sed odio.</p><p>Aenean et tortor at risus viverra adipiscing at. Sagittis id consectetur purus ut. Ullamcorper sit amet risus nullam eget. Est sit amet facilisis magna etiam. Arcu dui vivamus arcu felis bibendum ut tristique. Sed odio morbi quis commodo odio aenean sed adipiscing.</p>",
                commands: [{
                    command: "--urna <urna >",
                    text: "urna feugiat",
                },
                {
                    command: "--morbi <morbi >",
                    text: "Ullamcorper sit amet risus nullam eget",
                },
                {
                    command: "--expl <expelleriamusr>",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                }, {
                    command: "--crucio <cruciatus>",
                    text: "Suspendisse sed nisi lacus sed viverra tellus",
                },
                {
                    command: "---expecto_patronum",
                    text: "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit.",
                },
                {
                    command: "--no_accio",
                    text: "Gravida in fermentum et sollicitudin ac orci phasellus egestas tellus",
                },
                {
                    command: "-n, --nisi",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },
                {
                    command: "--nisi_all",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                }
                ],
                isFav: true,
                isActive:false
            },
            {
                id: '8',
                name: "List Item 8",
                labels: [
                    {
                        title: 'Label #16',
                        id: 45989553676,
                    },
                    {
                        title: 'Label #23',
                        id: 32433433432445,
                    }
                ],
                owner: "Nebt",
                lastUpdated: "02/23/2017",
                createdBy: "Dr",
                createdDate: "11/07/2018",
                source: "C:/Users/Deskptop/morbi.py",
                usage: "morbi.py [...args...]",
                sampleInvocation: [
                    "To make all pellentesque habitant hjkfh ioueuieu nldhnksdj k morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna Urna_leo_convallis.py label1-ejkwjekjw",
                    "To restore kjdgj jdhjhuwj dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit Urna_leo_convallis.py label2 --urna jklrjekrjejjj kjk jk "
                ],
                desc: "<p>Lorem ipsum dolor  hkhf  jklj kfj uoiwueu woie89 08e0- 290 289 -09 e-0w 09 0wkel  sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci eu lobortis elementum nibh tellus. Condimentum id venenatis a condimentum vitae. In dictum non consectetur a erat nam at lectus urna. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Parturient montes nascetur ridiculus mus mauris. Auctor augue mauris augue neque gravida in. Sed augue lacus viverra vitae congue eu consequat ac. Bibendum neque egestas congue quisque egestas diam in arcu. Sit amet porttitor eget dolor morbi non arcu risus quis. Leo vel orci porta non pulvinar neque laoreet suspendisse. Congue nisi vitae suscipit tellus.</p><p>Dignissim convallis aenean et tortor at. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Volutpat maecenas volutpat blandit aliquam etiam. Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Sem integer vitae justo eget magna fermentum. Sociis natoque penatibus et magnis dis. Sem integer vitae justo eget. Mi ipsum faucibus vitae aliquet. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Orci eu lobortis elementum nibh. At quis risus sed vulputate. Adipiscing elit ut aliquam purus sit amet luctus venenatis. Aliquam eleifend mi in nulla posuere sollicitudin. Odio eu feugiat pretium nibh ipsum consequat nisl. A lacus vestibulum sed arcu non odio euismod lacinia. Eget duis at tellus at urna condimentum mattis pellentesque id. Mollis nunc sed id semper risus in hendrerit gravida. Velit dignissim sodales ut eu sem integer vitae justo. Quis vel eros donec ac odio. Morbi leo urna molestie at elementum eu facilisis sed odio.</p><p>Aenean et tortor at risus viverra adipiscing at. Sagittis id consectetur purus ut. Ullamcorper sit amet risus nullam eget. Est sit amet facilisis magna etiam. Arcu dui vivamus arcu felis bibendum ut tristique. Sed odio morbi quis commodo odio aenean sed adipiscing.</p>",
                commands: [{
                    command: "--expl <expelleriamusr>",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--morbi <morbi >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--urna <urna >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                }, {
                    command: "--crucio <cruciatus>",
                    text: "Ullamcorper sit amet risus nullam eget",
                },
                {
                    command: "---expecto_patronum",
                    text: "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit.",
                },
                {
                    command: "--no_accio",
                    text: "Lectus vestibulum mattis ullamcorper velit seds",
                },
                {
                    command: "-n, --nisi",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },
                {
                    command: "--nisi_all",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },

                ],
                isFav: false,
                isActive:false
            },
            {
                id: '9',
                name: "List Item 9",
                labels: [
                    {
                        title: 'Label #12',
                        id: 3232656568787,
                    },
                    {
                        title: 'Label #13',
                        id: 898856567867,
                    }
                ],
                owner: "Reny",
                lastUpdated: "11/3/2013",
                createdBy: "Mark",
                createdDate: "10/09/2016",
                source: "C:/Users/Deskptop/morbi.py",
                usage: "morbi.py [...args...]",
                sampleInvocation: [
                    "To make all pellentesque habitant hjkfh ioueuieu nldhnksdj k morbi tristique senectus et netus et malesuada fames ac turpis egestas maecenas pharetra convallis posuere morbi leo urna Urna_leo_convallis.py label1-ejkwjekjw",
                    "To restore kjdgj jdhjhuwj dictum varius duis at consectetur lorem donec massa sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit Urna_leo_convallis.py label2 --urna jklrjekrjejjj kjk jk "
                ],
                desc: "<p>Lorem ipsum dolor  hkhf  jklj kfj uoiwueu woie89 08e0- 290 289 -09 e-0w 09 0wkel  sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci eu lobortis elementum nibh tellus. Condimentum id venenatis a condimentum vitae. In dictum non consectetur a erat nam at lectus urna. Faucibus scelerisque eleifend donec pretium vulputate sapien nec. Parturient montes nascetur ridiculus mus mauris. Auctor augue mauris augue neque gravida in. Sed augue lacus viverra vitae congue eu consequat ac. Bibendum neque egestas congue quisque egestas diam in arcu. Sit amet porttitor eget dolor morbi non arcu risus quis. Leo vel orci porta non pulvinar neque laoreet suspendisse. Congue nisi vitae suscipit tellus.</p><p>Dignissim convallis aenean et tortor at. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida neque. Volutpat maecenas volutpat blandit aliquam etiam. Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Sem integer vitae justo eget magna fermentum. Sociis natoque penatibus et magnis dis. Sem integer vitae justo eget. Mi ipsum faucibus vitae aliquet. Tempus iaculis urna id volutpat lacus laoreet non curabitur. Orci eu lobortis elementum nibh. At quis risus sed vulputate. Adipiscing elit ut aliquam purus sit amet luctus venenatis. Aliquam eleifend mi in nulla posuere sollicitudin. Odio eu feugiat pretium nibh ipsum consequat nisl. A lacus vestibulum sed arcu non odio euismod lacinia. Eget duis at tellus at urna condimentum mattis pellentesque id. Mollis nunc sed id semper risus in hendrerit gravida. Velit dignissim sodales ut eu sem integer vitae justo. Quis vel eros donec ac odio. Morbi leo urna molestie at elementum eu facilisis sed odio.</p><p>Aenean et tortor at risus viverra adipiscing at. Sagittis id consectetur purus ut. Ullamcorper sit amet risus nullam eget. Est sit amet facilisis magna etiam. Arcu dui vivamus arcu felis bibendum ut tristique. Sed odio morbi quis commodo odio aenean sed adipiscing.</p>",
                commands: [{
                    command: "--expl <expelleriamusr>",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--morbi <morbi >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                },
                {
                    command: "--urna <urna >",
                    text: "Lectus vestibulum mattis ullamcorper velit sed",
                }, {
                    command: "--crucio <cruciatus>",
                    text: "Ullamcorper sit amet risus nullam eget",
                },
                {
                    command: "---expecto_patronum",
                    text: "Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit.",
                },
                {
                    command: "--no_accio",
                    text: "Lectus vestibulum mattis ullamcorper velit seds",
                },
                {
                    command: "-n, --nisi",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },
                {
                    command: "--nisi_all",
                    text: "Curabitur vitae nunc sed velit dignissim, nisi quis eleifend quam adipiscing.",
                },

                ],
                isFav: false,
                isActive:false
            }
        ]
    });
    const [search, setSearch] = useState('');
    const [filteredScripts, setFilteredScripts] = useState([]);
    const [allLabels, setAllLabels] = useState([]);
    const [labelFilter, setLabelFilter] = useState('');
    const [sidebarStatus, setSidebarStatus] = useState();
    const [favFlagStatus, setFavFlagStatus] = useState(false);
    
    let scriptItems = [];
    //let allLabels = [];
    const changeData=true;
    useEffect(() => {
        {/* to set the first script as active in the list */ }
        props.onSelectScript(scriptsData.scripts[0]);

        /* Collecting all the lables */
        for (let script of scriptsData.scripts) {
            for (let label of script.labels) {
                label.isChecked = true;
                const found = allLabels.some(labelObj => labelObj.id === label.id);
                if (!found) {
                    allLabels.push(label);
                }
            }
        }
    }, [changeData])

    useEffect(() => {
        {/* to search and filter the scripts based on the script name and labels */ }
        setFilteredScripts(
            scriptsData.scripts.filter(script => {
                if (script.name.toLowerCase().includes(search.toLowerCase()) && isLabelMatchedWithScript(script)) {
                    return true;
                }
            })
        )
    }, [search])

    {/* to hanldle the changed value of the checkboxes */ }
    function handleLabelChange(label, e) {
        var index = allLabels.findIndex(obj => obj.id === label.id);

        let allLabelsNew = allLabels;
        allLabelsNew[index].isChecked = e.target.checked;
        setAllLabels(allLabels)
    }

    {/*to generate label elements */ }
    let allLabelsElemnts = [];
    allLabelsElemnts = allLabels.map(label => {
        return (
            <li key={label.id} className="nav-item text-xs d-flex align-items-center">
                <input key={label.id} type="checkbox" id={"label" + label.id} name={"label" + label.id} defaultChecked={label.isChecked} onChange={(e) => handleLabelChange(label, e)} />
                <label className="ml-1" htmlFor={"label" + label.id}>{label.title}</label>
            </li>
        )
    })

    {/* to check whether labels in scripts are selected in filters or not */ }
    function isLabelMatchedWithScript(script) {
        let isLabelMatched = false;
        if (script.labels && script.labels.length > 0) {
            for (let label of script.labels) {
                for (let lableObj of allLabels) {
                    if ((label.id == lableObj.id) && lableObj.isChecked) {
                        isLabelMatched = true;
                        return true;
                    }
                }
            }
        }
        return isLabelMatched;
    }
    /* const filteredScripts = scriptsData.scripts.filter(script => {
        if ((script.name.toLowerCase().includes(search.toLowerCase()) && isLabelMatchedWithScript(script))) {
            return true;
        }
    }) */

    {/* to filter the scripts based on the labels */ }
    function filterByCheckLabels() {
        const filteredScripts = scriptsData.scripts.filter(script => {
            if (((script.name.toLowerCase().includes(search.toLowerCase())) && isLabelMatchedWithScript(script) && (favFlagStatus?((script.isFav==favFlagStatus)):true))) {
                return true;
            }
        })
        //setAllLabels(allLabels);
        setFilteredScripts(filteredScripts)
    }

    {/* to generate the scripts list using the array of items */ }
    scriptItems = filteredScripts.map(script => {
        return (
            <Nav.Item key={script.id} className="d-flex align-items-center justify-content-between">
                <Nav.Link href={"/"+script.id} className={script.isActive ? 'py-2 px-3 mb-1 w-100 d-flex align-items-center justify-content-between active' : 'py-2 px-3 w-100 d-flex align-items-center justify-content-between'} 
 onClick={(e) => setActiveScript(script, e)}>
                    <div>
                        <div>{script.name}</div>
                        {script.labels.map(label => (
                            <Badge variant="primary" key={label.id} className="d-inline-block bg-light-blue mr-1">{label.title}</Badge>
                        ))}
                    </div>
                    {script.isFav ? <i className="fa fa-star text-amber mr-2" aria-hidden="true"></i> : ''}
                </Nav.Link>
            </Nav.Item>
        );
    })

    {/* to make the clicked script in sidebar as active scrip in order to show the details of the script */ }
    function setActiveScript(script, e) {
        e.preventDefault();
        for(let scriptObj of scriptsData.scripts){
            scriptObj.isActive=false;
            if(scriptObj.id === script.id){
                scriptObj.isActive=true;
            }
        }
        //var activeScriptIndex = scriptsData.scripts.findIndex(obj => obj.id === script.id);

        //scriptsData.scripts[activeScriptIndex].isActive=true;
        
        props.onSelectScript(script);
        setScripts(scriptsData);

    }

    {/* to slect all the labels */ }
    function selectAllLabels(e){
        for(let labelObj of allLabels){
            labelObj.isChecked=true; 
        }
        setAllLabels(allLabels);
    }

    {/* to slect no labels */ }
    function selectNoLabels(e){
        for(let label of allLabels){
            label.isChecked=false; 
        }
        
        setAllLabels(allLabels);
    }

    {/* to filter scripts based on the favourite items */ } 
    function filterByFavItem(e){
        for(let label of allLabels){
            label.isChecked=false; 
        }
        
        setAllLabels(allLabels);
    }

    {/* to set the favourite checkbox on */ } 
    function handleFavChange(e){
        setFavFlagStatus(e.target.checked);
    }

    {/* to reset all filters */ } 
    function resetAllFilters(){
        for(let label of allLabels){
            label.isChecked=true; 
        }
        setFavFlagStatus(false);
        setAllLabels(allLabels);
    }
    return (
        <div className="side-bar bg-dark position-absolute py-2 w-100">

            {/* Side bar Header */}
            <div className="side-bar-header d-flex align-items-center justify-content-between px-3 mb-2">
                <div className="d-flex align-items-center">
                    <i className="fa fa-microchip text-white" aria-hidden="true"></i>
                    <h2 className="text-white text-base side-bar-head font-weight-normal ml-2 mb-0">Scripts Finder</h2>
                </div>
                <span className="d-flex align-items-center">
                    <img src="https://res-3.cloudinary.com/crunchbase-production/image/upload/c_thumb,h_256,w_256,f_auto,g_faces,z_0.7,q_auto:eco/efdchffqiwq5oyn4x3o4" width="20" className="rounded-circle" />
                    <a href="#" onClick={() => setSidebarStatus(!sidebarStatus)}>
                        {sidebarStatus ? <i className="fa fa-times text-white ml-2 d-lg-none text-sm" aria-hidden="true"></i> : <i className="fa fa-bars text-white ml-2 d-lg-none text-sm" aria-hidden="true"></i>}
                    </a>
                </span>
            </div>
            {/* End Side bar Header */}

            {/* Side bar Body */}
            <div className={"side-bar-body py-2 " + (sidebarStatus ? 'd-block' : 'd-none')} >
                <div className="d-flex align-items-center justify-content-between px-3">
                    <InputGroup className="mb-2 mr-3 search-script-finder">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="" className="border-0"><i className="fa fa-search text-sm" aria-hidden="true"></i></InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            placeholder="Search" className="text-xs border-0 p-0 h-auto" onChange={e => setSearch(e.target.value)}
                        />
                    </InputGroup>

                    {/* Filter scripts Dropdown  */}
                    <Dropdown as={ButtonGroup} className="position-relative filter-dropdown">

                        <Dropdown.Toggle split variant="success" className="bg-transparent p-0 border-0 shadow-none">
                            <i className="fa fa-filter text-white text-xl" aria-hidden="true"></i>
                            <span className="scripts-counts text-white bg-danger position-absolute font-weight-old">2</span>
                        </Dropdown.Toggle>

                        <Dropdown.Menu className="p-3 bg-brand" style={{ minWidth: "210px" }}>
                            <h6 className="text-xs">Filter By Label </h6>
                            <div className="bg-white mb-3">

                                {/* Labels Dropdown List  */}
                                <ul className="labels-list nav flex-column p-2 border-bottom">
                                    {allLabelsElemnts}
                                </ul>
                                {/* End  Labels Dropdown List  */}

                                <div className="d-flex align-items-center p-2">
                                    <span className="text-xs">Select:</span>
                                    <div className="">
                                        <a href="#" className="text-xs text-blue mx-2" onClick={(e) => selectAllLabels(e)}>All</a>
                                        <span className="text-xs">|</span>
                                        <a href="#" className="text-xs text-blue mx-2" onClick={(e) => selectNoLabels(e)}>None</a>
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex align-items-center mb-4">
                                <input type="checkbox" name="vehicle1" className="mr-2"  onChange={(e) => handleFavChange(e)}/>
                                <i className="fa fa-star text-amber mr-2" aria-hidden="true"></i>
                                <span className="">Show Only My Favorites</span>
                            </div>
                            <Button variant="info" className="d-block w-100 text-xs bg-medium-blue mb-3" onClick={(e) => filterByCheckLabels(e)}>APPLY</Button>
                            <Button variant="link" className="d-block w-100 text-xs py-0 text-blue" onClick={(e) => resetAllFilters(e)}>RESET ALL FILTERS</Button>
                        </Dropdown.Menu>
                    </Dropdown>
                    {/*End Filter scripts Dropdown  */}

                </div>

                {/* Scripts List */}
                <div className="scripts-list">
                    <Nav className="flex-column">
                        {scriptItems}
                    </Nav>
                </div>
                {/* Scripts List */}

            </div>

            {/*End Side bar Body */}

        </div>
    );
}

export default Sidebar;
