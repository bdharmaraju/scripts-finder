import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';


function ScriptActions(props) {

    return (

         /* Script actions section */
        <div className="script-actions mr-kg-5 my-3 mx-3 ml-lg-0">
            <Button variant="info" className="d-block w-100 text-xs text-left bg-medium-blue mb-4">
                <i className="fa fa-play mr-1" aria-hidden="true"></i>
                Run This Script
            </Button>
            <ul className="nav flex-column ml-3">
                <li>
                    <a href="#" className="text-blue d-block mb-2">
                        <i className="fa fa-share-alt text-dark mr-1" aria-hidden="true"></i>
                Share
            </a>
                </li>
                <li>
                    <a href="#" className="text-blue d-block mb-2">
                        <i className="fa fa-code text-dark mr-1" aria-hidden="true"></i>
                Copy Path
            </a>
                </li>
                <li>
                    <a href="#" className="text-blue d-block mb-2">
                        <i className="fa fa-gitlab text-dark mr-1" aria-hidden="true"></i>
               Open In Gitlab
            </a>
                </li>
                <li>
                    <a href="#" className="text-blue d-block mb-2">
                        <i className="fa fa-opera text-dark mr-1" aria-hidden="true"></i>
                Explore On OpenGrok
            </a>
                </li>
                <li>
                    <a href="#" className="text-blue d-block mb-2">
                        <i className="fa fa-pencil text-dark mr-1" aria-hidden="true"></i>
                Edit Script Metadata
            </a>
                </li>
            </ul>
        </div>
    );
}

export default ScriptActions;