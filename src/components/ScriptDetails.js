import React, { useState } from 'react';
import Badge from 'react-bootstrap/Badge';
import { Container, Row, Col, Table } from 'react-bootstrap';
import addNotification from 'react-push-notification';

function ScriptDetails(props) {

    {/* to mark the script as favourite*/ }
    function markScriptFav(script, isFav, e) {
        e.preventDefault();
        props.onScriptEdit(script, isFav);

        {/* to add the push notification*/}
        addNotification({
            title: 'Warning',
            subtitle: 'This is a subtitle',
            message: 'You have a notification',
            theme: 'darkblue',
            insert: "top",
            position: 'top-right',
            closeButton: 'Go away',
            native: false // when using native, your OS will handle theming.
        });
    }
    
    return (
        <div className="script-view mx-3 mx-lg-3 my-3">
            <div className="d-flex">
                <div className="script-view-icon mt-5 mt-lg-0 d-none d-lg-block">
                    <i className="fa fa-files-o" aria-hidden="true"></i>
                </div>
                <div className="script-view-info">
                    {/* Script View Header */}
                    <div className="script-view-header mt-5 mt-lg-0">
                        <Row>
                            <Col sm="4" className="col">
                                <div className="d-flex align-items-start">
                                    <i className="fa fa-files-o text-lg mr-2 d-block d-lg-none" aria-hidden="true"></i>
                                    <div className="mb-3">
                                        <h2 className="text-base side-bar-head font-weight-normal mb-0">{props.selectedScript && props.selectedScript.name ? props.selectedScript.name : ''}</h2>

                                        {props.selectedScript && props.selectedScript.labels && props.selectedScript.labels.map(label => (

                                            <Badge variant="primary" key={label.id} className="d-inline-block bg-light-blue mr-1">{label.title}</Badge>

                                        ))}
                                    </div>
                                </div>
                            </Col>
                            <Col sm="8" className="col">
                                <a href="#" className="text-blue" onClick={(e) => markScriptFav(props.selectedScript, !props.selectedScript.isFav, e)}><i className={(props.selectedScript && props.selectedScript.isFav) ? 'fa fa-star mr-2 text-amber' : 'fa fa-star mr-2 text-white'} aria-hidden="true"></i>Mark As Favorite</a>
                            </Col>
                        </Row>

                        <p className="mb-0">Owner: {(props.selectedScript && props.selectedScript.owner) ? props.selectedScript.owner : ''} | Last updated: {(props.selectedScript && props.selectedScript.lastUpdated) ? props.selectedScript.lastUpdated : ''}</p>
                        <p className="mb-0">Created by: {(props.selectedScript && props.selectedScript.createdBy) ? props.selectedScript.createdBy : ''} on {(props.selectedScript && props.selectedScript.createdDate) ? props.selectedScript.createdDate : ''}</p>
                        <p className="mb-0">Source: <a href="#" className="text-blue">{(props.selectedScript && props.selectedScript.source) ? props.selectedScript.source : ''}</a></p>
                    </div>
                    {/* End Script View Header */}

                    <hr></hr>

                    {/* Script View Body */}
                    <div className="script-view-body">
                        <p><span className="font-weight-500">Usage:</span> {(props.selectedScript && props.selectedScript.usage) ? props.selectedScript.usage : ''}</p>
                    </div>

                    {/* Sample invocation: */}
                    <h3 className="text-base">Sample invocation:</h3>
                    <Row className="mx-0">
                        <Col xs md="9" xl="7" className="px-0">
                            <ul className="pl-3">
                                {props.selectedScript && props.selectedScript.sampleInvocation.map((item, index) => (
                                    <li key={index}><p>{item}</p></li>
                                ))}
                            </ul>
                        </Col>
                    </Row>
                    {/* End Sample invocation: */}

                    {/* Description */}
                    <h3 className="text-base">Description:</h3>
                    <p dangerouslySetInnerHTML={{ __html: props.selectedScript && props.selectedScript.desc }} ></p>
                    {/*End Description */}

                    {/* Commands */}
                    <h3 className="text-base">Commands:</h3>
                    <Table bordered className="bg-white">
                        <tbody>
                            {props.selectedScript && props.selectedScript.commands.map((item, index) => (
                                <tr key={index}>
                                    <td className="py-2">{item.command}</td>
                                    <td className="py-2">{item.text}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                    {/*End Commands */}

                    {/*End Script View Body */}
                </div>
            </div>
        </div>
    );
}

export default ScriptDetails;
