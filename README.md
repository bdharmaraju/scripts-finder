# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Script Finder Interface is for listing the Scripts, search and view its details
* It has some sections like Usage, Sample Invocation, Description Brief description about the script, Command options 

### How do I get set up? ###

Refer to https://nodejs.org/en/ to install nodejs

### Install create-react-app
Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

npm install -g create-react-app

Install react bootstrap using the followig command

npm install react-bootstrap bootstrap

To run the Project need to eneter npm start in the terminal

Need to load the required modules and create components.

## Live Application URL

The Application is deployed at git@bitbucket.org:bdharmaraju/scripts-finder.git
